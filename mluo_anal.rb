#!/usr/bin/env ruby

require 'date'
require 'csv'

file = ARGV[0]

line_num=0

# open parter file and seperate new lines
text=File.open(file).read
text.gsub!(/\r\n?/, "\n")

# admin paths will store links in namespace admin
# paths will store links that are not in namespace
#[
#{name: "path=/", count: 15, id: 1, dates: [], fatal_count: 10}, 
#{name: "path=/countries", count: 10, id: 2, dates: [], fatal_count: 10}, 
#...]
#


paths = []
admin_paths = []

def strip_chars(w)
  return w.gsub(")", "").gsub(":", "")
end

=begin
grep -E 'pickem|playstation5|humanitarnautakmica|groups|ranking|eliga|hrvatski-telekom-e-liga|gaming|velika-promjena-u-poretku-top-4-igraca|ulazimo-u-zadnji-tjedan-doigravanja|novi-tjedan-novi-rezultati|velike-promjene-na-ljestvici-top-16-igraca|drugi-tjedan-doigravanja-donosi-novu-ljestvicu|odigran-prvi-tjedan-doigravanja|eliga' nginx-log | grep -iv 'assets' | grep -iv 'uploads' > final_log_4
=end

# read file line by line ...
text_arr = text.split("\n")
# Between 1st [ and 1st #:
str1_time = "\["
str2_time = "\#"
url_regex = /GET(.*?)"/

calls_by_days = []
uniq_urls = []

str1_path = "GET"
str2_path = "for"

text_arr.each do |line|
  line_num += 1
  link = line[/#{str1_path}(.*?)#{str2_path}/m, 1]
  next unless link

  uniq_urls << link unless uniq_urls.include?(link)

  begin
    date = line[/\[(.*?)\#/m, 1]
    line_date = Date.parse( date )
  rescue
    next
  end
  line_date_s = line_date.to_s

  current_day = calls_by_days.find{|c| c[line_date_s]}

  if current_day
    calls_by_days.map do |c| 
      if current_day == c 
        c[line_date_s] =  c[line_date_s] + 1 
      end
    end
  else
    calls_by_days << { line_date_s => 1 }
  end

=begin
  # ... and word by word
  line_arr = line.split(" ")
  line_arr.each_with_index do |word_get, index|

    # if line has a substring path
    if word_get.include? 'GET'
      word = line_arr[index+1]

      # find dates for each link
      date_string = line.split(" ")[1][1..-1]
      begin
        date = Date.parse(date_string)
      rescue ArgumentError
        date = nil
      end

      # add_to_array can be admin_paths or paths
      add_to_array = word.include?("admin") ? admin_paths : paths

      # only in the next link fatal error can befound
      begin
        next_line = text_arr[line_num]
      rescue ArgumentError
        next_line = nil
      end
      is_error = next_line ? next_line[0] == "F" : nil

      # if path is already recorded update old record
      if add_to_array.map{|p| strip_chars(p[:name])}.include? strip_chars(word)

        # find existing record
        old_path = add_to_array.detect {|f| strip_chars(f[:name]) == strip_chars(word) }
        old_dates = old_path[:dates] << date
        new_path = {
          id: old_path[:id], 
          path: strip_chars(word), 
          count: old_path[:count] += 1,
          dates: date ? old_dates : old_path[:dates],
          success_count: !is_error ? old_path[:success_count] += 1 : old_path[:success_count],
          success_dates: (!is_error and date) ? old_dates : old_path[:dates], 
          fatal_count: is_error ? old_path[:fatal_count] += 1 : old_path[:fatal_count],
          fatal_dates: (is_error and date) ? old_dates : old_path[:dates]  
        }

        # update record in array
        add_to_array.map { |x| x[:id] == old_path[:id] ? new_path : old_path  }
      else
        # if path is unknow add it
        new_path = {
          id: line_num,
          name: strip_chars(word),
          count: 1,
          dates: date ? [date] : [],
          success_count: !is_error ? 1 : 0,
          success_dates: (!is_error and date) ? [date] : [],
          fatal_count: is_error ? 1 : 0,
          fatal_dates: (is_error and date) ? [date] : []  
        } 
        add_to_array << new_path
      end
    end

  end
=end

end

#puts calls_by_days
#puts ""
puts uniq_urls# <- a lot of urls bc they get some hashes

#puts paths.detect {|f| f[:name] == "path=/countries/11-russia" }[:dates]
#puts "+++ path: #{p[:name]}, times: #{p[:count]} | success: #{p[:success_count]} | fatal: #{p[:fatal_count]} "


CSV.open("result.csv", "w") do |csv|
  csv << ["path", "times open", "success", "fatal"]
  paths.each do |p|
    csv << [p[:name], p[:count], p[:success_count], p[:fatal_count]]
  end
end
