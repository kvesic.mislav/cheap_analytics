#!/usr/bin/env ruby

require 'date'
require 'csv'

file = ARGV[0]

line_num=0

# open parter file and seperate new lines
text=File.open(file).read
text.gsub!(/\r\n?/, "\n")

# admin paths will store links in namespace admin
# paths will store links that are not in namespace
#[
#{name: "path=/", count: 15, id: 1, dates: [], fatal_count: 10}, 
#{name: "path=/countries", count: 10, id: 2, dates: [], fatal_count: 10}, 
#...]

paths = []
admin_paths = []

def strip_chars(w)
  return w.gsub(")", "").gsub(":", "")
end

# read file line by line ...
text_arr = text.split("\n")
text_arr.each do |line|
  line_num += 1

  # ... and word by word
  line_arr = line.split(" ")
  line_arr.each_with_index do |word_get, index|

    # if line has a substring path
    if word_get.include? "GET"
      # word after GET should be path
      word = line_arr[index+1]

      # find dates for each link
      date_string = line.split(" ")[1][1..-1]
      begin
        date = Date.parse(date_string)
      rescue ArgumentError
        date = nil
      end

      # add_to_array can be admin_paths or paths
      add_to_array = word.include?("admin") ? admin_paths : paths

      # only in the next link fatal error can befound
      begin
        next_line = text_arr[line_num]
      rescue ArgumentError
        next_line = nil
      end
      is_error = next_line ? next_line[0] == "F" : nil

      # if path is already recorded update old record
      if add_to_array.map{|p| strip_chars(p[:name])}.include? strip_chars(word)

        # find existing record
        old_path = add_to_array.detect {|f| strip_chars(f[:name]) == strip_chars(word) }
        old_dates = old_path[:dates] << date
        new_path = {
          id: old_path[:id], 
          path: strip_chars(word), 
          count: old_path[:count] += 1,
          dates: date ? old_dates : old_path[:dates],
          success_count: !is_error ? old_path[:success_count] += 1 : old_path[:success_count],
          success_dates: (!is_error and date) ? old_dates : old_path[:dates], 
          fatal_count: is_error ? old_path[:fatal_count] += 1 : old_path[:fatal_count],
          fatal_dates: (is_error and date) ? old_dates : old_path[:dates]  
        }

        # update record in array
        add_to_array.map { |x| x[:id] == old_path[:id] ? new_path : old_path  }
      else
        # if path is unknow add it
        new_path = {
          id: line_num,
          name: strip_chars(word),
          count: 1,
          dates: date ? [date] : [],
          success_count: !is_error ? 1 : 0,
          success_dates: (!is_error and date) ? [date] : [],
          fatal_count: is_error ? 1 : 0,
          fatal_dates: (is_error and date) ? [date] : []  
        } 
        add_to_array << new_path
      end
    end

  end

end

#puts paths.detect {|f| f[:name] == "path=/countries/11-russia" }[:dates]
#puts "+++ path: #{p[:name]}, times: #{p[:count]} | success: #{p[:success_count]} | fatal: #{p[:fatal_count]} "


CSV.open("result.csv", "w") do |csv|
  csv << ["path", "times open", "success", "fatal"]
  paths.each do |p|
    csv << [p[:name], p[:count], p[:success_count], p[:fatal_count]]
  end
end
