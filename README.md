# Useage
- Download new or old cheap_analytics.rb in some folder
- Download logs to the same folder (scp -P 12321 deployer@production4.bambooapp.eu:/home/deployer/apps/goodgame.dev.bambooapp.eu/log/production.log /home/mislav/Desktop/prod.log)
- run: 
```
    ruby cheap_analytics.rb  prod.log
```
or
```
    ruby new_cheap_analytics.rb  prod.log
```

Results should be in the same folder with name: result.csv
